# Importation
from random import *

# Grille sur console 
def creation_grille():
    """
    Crée une grille vierge
    """
    global cell 
    haut = 5 # hauteur du tableau
    larg = 5  # largeur du tableau
   
    cell = [[0 for ligne in range(larg)] for tableau in range(haut)] # liste en deux dimensions 
    for hauteur in range(5):
        for largeur in range(5):
            print(cell[hauteur][largeur],end='\t')
    
    
def tour():
    """
    Tour comprenant la determination du nombre de voisins vivants de chaque cellule et l'application des regles
    """
    etat=[[0 for ligne in range(5)] for tableau in range(5)] # liste en deux dimensions 
    for ligne in range(5):
        for col in range(5):
            etat[ligne][col]=cell[ligne][col]
    
    # Vérification ligne par ligne, colonne par colonne du nb de voisins de chaque cellule       
    temp= [[0 for ligne in range(5)] for tableau in range(5)] # liste en deux dimensions
    nb_voisins=0
    for ligne in range(5):
        for col in range(5):
            # Conditions aux limites périodiques (pas tout compris encore)
            # La méthode utilisée est le modulo de 5 qui donne = 0
            # il permet d'effectuer les vérifications si une cellule est collée sur un bord de la grille
            if etat[ligne][(col-1)%5]==1:
                nb_voisins+=1
            if etat[ligne][(col+1)%5]==1:
                nb_voisins+=1
            if etat[(ligne+1)%5][(col-1)%5]==1:
                nb_voisins+=1
            if etat[(ligne+1)%5][col]==1:
                nb_voisins+=1
            if etat[(ligne+1)%5][(col+1)%5]==1:
                nb_voisins+=1
            if etat[(ligne-1)%5][(col-1)%5]==1:
                nb_voisins+=1
            if etat[(ligne-1)%5][col]==1:
                nb_voisins+=1
            if etat[(ligne-1)%5][(col+1)%5]==1:
                nb_voisins+=1

            if etat[ligne][col] == 1 and nb_voisins < 2:                
                temp[ligne][col] = 0           
            if etat[ligne][col] == 1 and (nb_voisins == 2 or nb_voisins == 3):                
                temp[ligne][col] = 1          
            if etat[ligne][col] == 1 and nb_voisins > 3:                
                temp[ligne][col] = 0         
            if etat[ligne][col] == 0 and nb_voisins == 3:                
                temp[ligne][col] = 1
                
            cell[ligne][col] = temp[ligne][col]
            nb_voisins=0
            
    for hauteur in range(5):
        for largeur in range(5):
            print(cell[hauteur][largeur],end='\t')
    print()    


def config_aleatoire():
    """
    Pour remplir la grille de facon aléatoire
    """
    for hauteur in range(5):
        for largeur in range(5):
            cell[hauteur][largeur]=randint(0,1)
    for hauteur in range(5):
        for largeur in range(5):
            print(cell[hauteur][largeur],end='\t')
    print('\nConfiguration finie')
    
    
def config_manuelle():
    """
    Pour remplir la grille manuellement
    """
    nb_modif=int(input("Choisir un nb de cellules vivantes: "))

    for config in range(nb_modif):
        ligne=int(input("Quelle ligne ? "))
        colonne=int(input("Quelle colonne ? "))
        cell[ligne][colonne]=1
        # for hauteur in range(10):
        #     for largeur in range(10):
        #         print(cell[hauteur][largeur],end='\t')
    print("Configuration finie")

# Programme principal du jeu
def main():
    creation_grille()
    
    erreur=True # Tant que la config n'est pas choisie, =True
    while (erreur == True):
        config=input("\nChoisir une config aléatoire ou manuelle : ")
        # Choix entre la config manuelle ou aléatoire 
        if config=='aleatoire' :
            erreur=False
            config_aleatoire()
        elif config=='manuelle' :
            erreur=False
            config_manuelle()
        else:
            erreur=True
            
    # Ici le nombre de tours du jeu est fixé à 10 par défaut
    for longueur in range(10):
        tour()
        print('Tour n° : ',longueur+1)
    print("Fin de la partie")
    
if __name__ == '__main__':
    main()

