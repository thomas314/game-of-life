# Règles du jeu de la vie

---

**à T=0 deux cas possibles:**

- la cellule est allumée Si elle dispose de 2 ou 3 cellules allumées dans son voisinnage proche (8 voisines) alors elle reste allumée à T+1 et s'éteint sinon.
- la cellule est éteinte Si elle a exactement 3 voisines proches d'allumées alors elle s'allume sinon elle reste éteinte.
Réaliser en programmation fonctionnelle une simulation du jeu de la vie.

Le programme pourra être paramétré par fichier ou en argument concernant la taille de la grille (en x et en y) et savoir si les bords de la grille sont connectés entre eux.

Les configurations de base seront stockées dans des fichiers au format :

5 //taille en x
5 //taille en y
0 // wrap ou non
(3,1) //coordonnées des cellules actives
(3,2)
(3,3)
L'affichage est laissé à l'appréciation du/de la programmeur(se). Le format texte est encouragé.

--- 

# Spécifications à noter : 

- Je n'ai pas pu récupérer le fichier ***.txt*** avec la méthode loadtxt()
- Mon affichage n'est clairement **pas optimal**
- La question **de la gestion des bords n'a pas été comprise en sa totalité**, j'ai donc suivi des tutos pour appliquer le modulo et faire mes conditions de manière optimale.

# Comment ça marche ? 

Tout d'abord, pour lancer une partie, veuillez effectuer sur votre console : 

```python3 game.py```

Ensuite il vous sera demandé quelle configuration choisir (manuelle ou aléatoire).

Enfin, vous pourrez changer le nb de tours du jeu de la vie (l.117 du fichier ```game.py```) en changeant la portée des tours.

### Bon jeu ! 


**Thomas REVEL**